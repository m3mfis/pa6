package sample;

import com.freshcode.GameApi;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.Random;

public class Main extends Application
{

    public static Random random = new java.util.Random();

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
        primaryStage.setOnCloseRequest(windowEvent -> {
            Platform.exit();
            System.exit(0);
        });
    }

    public static void main(String[] args)
    {
        new Thread(Main::processOut).start();
        new Thread(Main::processInput).start();
        launch(args);
    }

    private static void processInput()
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true)
        {
            try
            {
                String inputLine = reader.readLine();
                GameApi.GameRequest gameRequest = GameApi.GameRequest.parseFrom(Base64.getDecoder().decode(inputLine.getBytes()));
                System.err.println(gameRequest);
                Platform.runLater(() -> Controller.events.add(0, "In:" + gameRequest.toString()));
                Thread.sleep(1000l);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    private static void processOut()
    {
        while (true)
        {
            GameApi.GameStatistics gameResult = GameApi.GameStatistics.newBuilder()
                    .setFidgets(random.nextInt() % 10)
                    .build();
            String result = Base64.getEncoder().encodeToString(gameResult.toByteArray());
            System.out.println(result);
            Platform.runLater(() -> Controller.events.add(0, "Out:" + gameResult.toString()));
            try
            {
                Thread.sleep(1000l);
            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
