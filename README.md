# Getting started

## Required

### Node.js

Download: [nodejs.org](https://nodejs.org/download/)

### NWjs 0.12.3 +

Home:   [nwjs.io](http://nwjs.io)

GitHub:   [nwjs/nw.js](https://github.com/nwjs/nw.js)


### Typescript 1.5 +
#### TypeScript Compiler

Home:   [TypeScript](http://www.typescriptlang.org/)

```
npm install -g typescript
```


#### TypeScript Definitely Typed

GitHub:   [DefinitelyTyped/tsd](https://github.com/DefinitelyTyped/tsd)

Home:   [definitelytyped.org/tsd/](http://definitelytyped.org/tsd/)

```
npm install -g tsd
```

#### TypeScript Formatter

GitHub:   [vvakame/typescript-formatter](https://github.com/vvakame/typescript-formatter)

```
npm install -g typescript-formatter
```

## Dependencies

In project root directory:

```
npm install
```

```
bower install
```

