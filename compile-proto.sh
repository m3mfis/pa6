#!/bin/bash

# generate java classes
protoc --java_out game1/src/main/java management/GameApi.proto

# generate ts classes
management/node_modules/protobufjs/bin/pbjs management/GameApi.proto > GameApi.json
management/node_modules/proto2typescript/bin/proto2typescript-bin.js --file GameApi.json > management/assets/typescript/api/GameApi.d.ts
rm -f GameApi.json
