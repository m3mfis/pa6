/// <reference path='../_all.ts' />

module app
{
    'use strict';

    export interface Process
    {
        sendMessage:(data:any) => void;
        setOnDataCallback(callback:(data:any) => void)
    }

}
