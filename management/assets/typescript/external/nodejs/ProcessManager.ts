/// <reference path='../../_all.ts' />

import child_process = require('child_process');

module ProcessManager
{
    'use strict';

    class NodeProcess implements app.Process
    {
        private callback:(data:any) => void;

        constructor(private process:child_process.ChildProcess)
        {
            process.stdout.on("data", this.processData);
            process.stderr.on("data", (data) => console.log(data.toString()));
            process.on('close', function (code)
            {
                if (code !== 0)
                {
                    console.log('ps process exited with code ' + code);
                }
            });
        }

        public sendMessage = (data) =>
        {
            console.log("send message: ", data);
            var builder:freshcode.GameRequestBuilder = dcodeIO.ProtoBuf.loadProtoFile('GameApi.proto').build('freshcode.GameRequest');
            var request:freshcode.GameRequest = new builder(data);
            this.process.stdin.write(request.toBase64() + "\n");
        };

        private processData = (data) =>
        {
            var builder:freshcode.GameStatisticsBuilder = dcodeIO.ProtoBuf.loadProtoFile('GameApi.proto').build('freshcode.GameStatistics');
            var processedData = data.toString().replace("\n", "");
            if (processedData.length)
            {
                var result:freshcode.GameStatistics = builder.decode64(processedData);
                if (this.callback)
                    this.callback(result);
            }
        };

        public setOnDataCallback = (callback) =>
        {
            this.callback = callback;
        };
    }

    export function startProcess():app.Process
    {
        console.log("start process");
        var process:child_process.ChildProcess = child_process.spawn('java', ['-jar', '../game1/target/import-1.0.0.jar']);
        return new NodeProcess(process);
    }

}
