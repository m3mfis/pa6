/// <reference path='../../_all.ts' />
var child_process = require('child_process');
var ProcessManager;
(function (ProcessManager) {
    'use strict';
    var NodeProcess = (function () {
        function NodeProcess(process) {
            var _this = this;
            this.process = process;
            this.sendMessage = function (data) {
                console.log("send message: ", data);
                var builder = dcodeIO.ProtoBuf.loadProtoFile('GameApi.proto').build('freshcode.GameRequest');
                var request = new builder(data);
                _this.process.stdin.write(request.toBase64() + "\n");
            };
            this.processData = function (data) {
                var builder = dcodeIO.ProtoBuf.loadProtoFile('GameApi.proto').build('freshcode.GameStatistics');
                var processedData = data.toString().replace("\n", "");
                if (processedData.length) {
                    var result = builder.decode64(processedData);
                    if (_this.callback)
                        _this.callback(result);
                }
            };
            this.setOnDataCallback = function (callback) {
                _this.callback = callback;
            };
            process.stdout.on("data", this.processData);
            process.stderr.on("data", function (data) { return console.log(data.toString()); });
            process.on('close', function (code) {
                if (code !== 0) {
                    console.log('ps process exited with code ' + code);
                }
            });
        }
        return NodeProcess;
    })();
    function startProcess() {
        console.log("start process");
        var process = child_process.spawn('java', ['-jar', '../game1/target/import-1.0.0.jar']);
        return new NodeProcess(process);
    }
    ProcessManager.startProcess = startProcess;
})(ProcessManager || (ProcessManager = {}));
