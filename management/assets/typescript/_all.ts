//#### Include type definitions ####
/// <reference path='typings/tsd.d.ts' />

//### Protobuf d.ts ###
/// <reference path='api/GameApi.d.ts' />

//### Translation tables ###
/// <reference path='Translate.ts' />

//#### Application ###
/// <reference path='Application.ts'/>

//#### External modules(node-specific) ###
/// <reference path='external/Process.ts'/>
/// <reference path='external/nodejs/ProcessManager.ts'/>

//#### Controllers ####
/// <reference path='controllers/LoginCtrl.ts' />
/// <reference path='controllers/ApplicationCtrl.ts' />
/// <reference path='controllers/ReviewCtrl.ts' />
/// <reference path='controllers/GamesCtrl.ts' />
/// <reference path='controllers/GameResultsCtrl.ts' />
/// <reference path='controllers/BehaviorInputCtrl.ts' />
/// <reference path='controllers/CurrentResultCtrl.ts' />
/// <reference path='controllers/RewardsCtrl.ts' />
/// <reference path='controllers/ProfileCtrl.ts' />

//#### Services ####
/// <reference path='services/UserService.ts' />
/// <reference path='services/DatabaseService.ts' />
/// <reference path='services/InitDBService.ts' />
