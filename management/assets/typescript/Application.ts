/// <reference path='_all.ts' />

declare var dcodeIO:any;
declare var Chartist:any;
declare var ProcessManager:any;

module app {
    'use strict';

    angular.module('app.controllers', ['lokijs',
        'ui.router',
        'ui.bootstrap',
        'rzModule',
        'ngSanitize',
        'angular-chartist',
        'pascalprecht.translate'])

        .service('userService', ()=> new UserService())
        .service('databaseService', ['Loki', (loki) => new DatabaseService(loki)])
        .service('initDBService', ['databaseService', (databaseService) => new InitDBService(databaseService)])

        .controller('ApplicationCtrl', ApplicationCtrl)
        .controller('ReviewCtrl', ReviewCtrl)
        .controller('GamesCtrl', GamesCtrl)
        .controller('RewardsCtrl', RewardsCtrl)
        .controller('ProfileCtrl', ProfileCtrl)
        .controller('GameResultsCtrl', GameResultsCtrl)
        .controller('BehaviorInputCtrl', BehaviorInputCtrl)
        .controller('CurrentResultCtrl', CurrentResultCtrl)
        .controller('LoginCtrl', LoginCtrl)

        .config(['$stateProvider', '$urlRouterProvider', '$translateProvider',
            function ($stateProvider, $urlRouterProvider, $translateProvider) {

                $translateProvider.translations('en', translationsEN);
                $translateProvider.preferredLanguage('en');


                $urlRouterProvider.otherwise('/login');
                $stateProvider
                    .state('login', {
                        url: "/login",
                        views: {
                            "app@": {
                                templateUrl: "assets/html/login/login.html",
                                controller: LoginCtrl
                            }
                        },
                        data: {
                            hideNav: true
                        }
                    })
                    .state('app', {
                        url: "/app",
                        views: {
                            "app@": {
                                templateUrl: "assets/html/app.html",
                                controller: ApplicationCtrl
                            }
                        }
                    })
                    .state('app.review', {
                        url: "/review",
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/review/review.html",
                                controller: ReviewCtrl
                            },
                            "header@app": {
                                templateUrl: "assets/html/review/review-header.html"
                            }
                        }
                    })
                    .state('app.games', {
                        url: "/games",
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/games/games.html",
                                controller: GamesCtrl
                            },
                            "header@app": {
                                templateUrl: "assets/html/games/games-header.html"
                            }
                        }
                    })
                    .state('app.games.game', {
                        url: '/:game',
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/games/game-results.html",
                                controller: GameResultsCtrl
                            },
                            "header@app": {
                                templateUrl: "assets/html/games/game-results-header.html"
                            }
                        }
                    })
                    .state('app.games.game.behavior', {
                        url: '/behavior-input',
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/games/behavior-input.html",
                                controller: BehaviorInputCtrl
                            }
                        },
                        data: {
                            hideNav: true
                        }
                    })
                    .state('app.games.game.currentResult', {
                        url: '/current-result',
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/games/current-results.html",
                                controller: CurrentResultCtrl
                            },
                            "header@app": {
                                templateUrl: "assets/html/games/current-results-header.html"
                            }
                        }
                    })
                    .state('app.rewards', {
                        url: "/rewards",
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/rewards/rewards.html",
                                controller: RewardsCtrl
                            },
                            "header@app": {
                                templateUrl: "assets/html/rewards/rewards-header.html"
                            }
                        }
                    })
                    .state('app.profile', {
                        url: "/profile",
                        views: {
                            "content@app": {
                                templateUrl: "assets/html/profile/profile.html",
                                controller: ProfileCtrl
                            },
                            "header@app": {
                                templateUrl: "assets/html/profile/profile-header.html"
                            }
                        }
                    })
            }])


        .run(['$rootScope', '$state', '$stateParams', 'initDBService',
            ($rootScope,
             $state,
             $stateParams,
             initDB:IInitDBService) => {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
                initDB.init(Mode.test);
            }])


}
