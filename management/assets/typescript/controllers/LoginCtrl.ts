/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface ILoginScope {
        login: string;
        password: string;

        signIn: Function;
        coach;
        coaches;
        assignCoach;
    }

    export class LoginCtrl {

        usersCollection;

        public static $inject = ['$scope', '$state','$stateParams', 'databaseService', 'userService'];
        constructor(private $scope:ILoginScope,
                    private $state,
                    private $stateParams,
                    private databaseService : IDatabaseService,
                    private userService :IUserService) {
            this.usersCollection = databaseService.getUsers();
            $scope.signIn = () => {
                var user = this.usersCollection.find({username: $scope.login})[0];

                if (user){
                    userService.setUser(user);
                    $state.go('app.review');
                }
            };

            $scope.assignCoach = (coach)=>{
                $scope.coach = coach;
            };

            $scope.coaches =
                [{name: 'Coach 1', id: 1},
                    {name: 'Coach 2', id: 2},
                    {name: 'Coach 3', id: 3}]
        }
    }
}
