/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IRewardsScope {

    }

    export class RewardsCtrl {

        public static $inject = ['$scope','$stateParams'];

        constructor(private $scope:IRewardsScope,
                    private $stateParams)
        {

        }
    }
}
