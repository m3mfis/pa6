/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IReviewScope {
        games : [Game];
    }

    export class ReviewCtrl {

        public static $inject = ['$scope'];

        constructor(private $scope :IReviewScope)
        {

        }
    }
}
