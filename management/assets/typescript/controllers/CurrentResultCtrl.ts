/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface ICurrentResultScope {
        game: Game;
        barData:any;
        options:any;
    }

    export class CurrentResultCtrl {

        public static $inject = ['$scope', '$stateParams', 'databaseService'];

        gamesCollection;

        options = {
            height: '95%',
            high: 10,
            low: 0,
            fullWidth: true,
            seriesBarDistance: -10,
            axisX: {
                showGrid: true
            },
            axisY: {
                showGrid: true,
                onlyInteger: true
            }
        };

        constructor(private $scope:ICurrentResultScope,
                    private $stateParams,
                    private databaseService:IDatabaseService) {
            this.gamesCollection = databaseService.getGames();
            $scope.game = this.gamesCollection.get($stateParams.game);
            $scope.options = this.options;
            $scope.barData = {
                labels: ['Attention Stamina', 'Visual Tracking', 'Academic Bridge', 'Time on Task'],
                series: [[5, 6, 7, 4], [5, 6, 7, 4]],
                color: ["#7CC63C", "#708ABC", "#708ABC", "#708ABC"],
                shadow: ["#6FB236", "#6581AE", "#6581AE", "#6581AE"]
            };
        }
    }
}
