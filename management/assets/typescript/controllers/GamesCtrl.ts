/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IGamesScope {
        games: [Game];
    }

    export class GamesCtrl {

        public static $inject = ['$scope', 'databaseService'];

        gamesCollection;

        constructor(private $scope:IGamesScope,
                    private databaseService : IDatabaseService) {
            this.gamesCollection = databaseService.getGames();
            $scope.games = this.gamesCollection.data;
        }
    }
}
