/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IGameResultsScope {
        game: Game;
        lineData;

        options;
    }

    export class GameResultsCtrl {

        public static $inject = ['$scope', '$stateParams', 'databaseService'];

        gamesCollection;

        options = {
            height: '92%',
            high: 50,
            low: 0,
            fullWidth: true,
            seriesBarDistance: -10,
            chartPadding: {
                right: 50
            },
            axisX: {
                showGrid: true
            },
            axisY: {
                showGrid: true,
                onlyInteger: true,
                labelInterpolationFnc: function (value, index) {
                    return value % 10 === 0 || value == 0 ? value : null;
                }
            },
            lineSmooth: false
        };

        constructor(private $scope:IGameResultsScope,
                    private $stateParams,
                    private databaseService:IDatabaseService) {
            this.gamesCollection = databaseService.getGames();
            $scope.game = this.gamesCollection.get($stateParams.game);

            $scope.lineData = {
                labels: ['30 June', '1 July', '2 July', '3 July', '4 July', ' '],
                series: [[15, 25, 20, 28, 24, 35]]
            };

            $scope.options = {
                height: '92%',
                high: 50,
                low: 0,
                fullWidth: true,
                seriesBarDistance: -10,
                chartPadding: {
                    right: 50
                },
                axisX: {
                    showGrid: true
                },
                axisY: {
                    showGrid: true,
                    onlyInteger: true,
                    labelInterpolationFnc: function (value, index) {
                        return value % 10 === 0 || value == 0 ? value : null;
                    }
                },
                lineSmooth: false
            };
        }
    }
}
