/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IBehaviorInputScope {
        game: Game;
        behaviors: any;
        startGame:Function;
    }

    export class BehaviorInputCtrl {

        public static $inject = ['$scope', '$stateParams'];

        constructor(private $scope:IBehaviorInputScope,
                    private $stateParams) {

            $scope.game = $stateParams.game;

            $scope.behaviors = [
                {
                    text: 'G_B_NERV_HAB',
                    value: 0,
                    min : 0,
                    max : 40
                },{
                    text: 'G_B_OFF_TASK',
                    value: 700,
                    min : 0,
                    max : 900
                },{
                    text: 'G_B_CANNOT_ADJ',
                    value: 50,
                    min : 0,
                    max : 100
                },{
                    text: 'G_B_CALLS_OUT',
                    value: 0,
                    min : 0,
                    max : 120
                },{
                    text: 'G_B_FIDGETS',
                    value: 0,
                    min : 0,
                    max : 100
                }
            ];

            $scope.startGame = this.startGame;

            console.log($stateParams,'behavior input');
        }

        startGame = () => {
            console.log("start game");
            var process:Process = ProcessManager.startProcess();
            setInterval(() => process.sendMessage({state:'STOP'}), 1000);
            process.setOnDataCallback(data => console.log(data));
        }
    }
}
