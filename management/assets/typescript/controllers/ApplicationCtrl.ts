/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IApplicationScope {
        isNode: boolean;
        user;
        coach;
    }

    export class ApplicationCtrl {

        public static $inject = ['$scope', '$state','$stateParams', '$rootScope', 'userService', 'databaseService'];

        usersCollection;

        constructor(private $scope:IApplicationScope,
                    private $state,
                    private $stateParams,
                    private $rootScope,
                    private userService :IUserService,
                    private databaseService: IDatabaseService) {
            this.usersCollection = databaseService.getUsers();

            $scope.user = userService.getUser();
            if(!$scope.user){
                $state.go('login');
            } else {
                $scope.coach = this.usersCollection.get($scope.user.coach);
            }
        }
    }
}
