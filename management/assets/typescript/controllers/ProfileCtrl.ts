/// <reference path='../_all.ts' />

module app {
    'use strict';

    interface IProfileScope {

    }

    export class ProfileCtrl {

        public static $inject = ['$scope','$stateParams'];

        constructor(private $scope:IProfileScope,
                    private $stateParams)
        {
        }
    }
}
