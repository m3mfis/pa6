module app {
    export var translationsEN = {
        G_B_NERV_HAB : 'Engages in nervous habits <br> (e.g. twists-hair, bite nails, chew objects)',
        G_B_OFF_TASK : 'Off task (e.g. eyes moving off target)',
        G_B_CANNOT_ADJ : 'Cannot adjust behavior  <br> to expectations of situation',
        G_B_CALLS_OUT : 'Calls out',
        G_B_FIDGETS : 'Fidgets'
    }
}