module app {

    export enum Mode {test}

    export interface IInitDBService {
        init(mode : Mode);
    }

    export class InitDBService implements IInitDBService {

        public static $inject = ['databaseService'];

        private users:LokiCollection<User>;
        private games:LokiCollection<Game>;

        constructor(private databaseService : IDatabaseService) {
            this.users = databaseService.getUsers();
            this.games = databaseService.getGames();
        }

        init = (mode) => {
            switch (mode) {
                case Mode.test:
                    this.users.removeDataOnly();
                    this.games.removeDataOnly();

                    this.users.insert([
                        {
                            username: "coach",
                            role: Role.coach,
                            password: '123',
                            firstName: 'Fernando',
                            lastName: "Lewis",
                            email: "some@mail",
                            phone: '777-77-77',
                            dateJoined: new Date()
                        }]);

                    this.users.insert([
                        {
                            username: "stud",
                            role: Role.student,
                            password: '123',
                            firstName: 'Julian',
                            lastName: "Davis",
                            email: "some1@mail",
                            phone: '777-77-77',
                            dateJoined: new Date(),
                            coach: this.users.data[0]['$loki'],
                            points: 27,
                            timeInPA: 123123,
                            timeInGames: 32133,
                            journalRequierd: true,
                            gamesPerSession: 2
                        }
                    ]);


                    this.games.insert([
                        {
                            name: 'Ocean Quest',
                            rating: 3,
                            badge: Badge.bronze,
                            attention: 75,
                            duration: "3:42",
                            recovery: 4,
                            impulsive: 2
                        },
                        {
                            name: 'Memory Lane',
                            rating: 4,
                            badge: Badge.silver,
                            attention: 50,
                            duration: "3:42",
                            recovery: 3
                        },
                        {
                            name: 'One more',
                            rating: 5,
                            badge: Badge.gold,
                            attention: 99,
                            duration: "3:42",
                            recovery: 6,
                            impulsive: 3
                        }]);

            }
        };
    }

}

