module app {

    export interface IUserService {
        setUser(user:User):void;
        getUser():User;
    }

    export class UserService implements IUserService{

        constructor(){}

        private user;

        setUser(user) {
            this.user = user;

            localStorage.setItem('user',JSON.stringify(user));
        }

        getUser() {
            this.user = JSON.parse(localStorage.getItem('user'));

            return this.user;
        }
    }

}

