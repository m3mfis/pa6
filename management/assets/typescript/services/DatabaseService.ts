module app {

    export enum Badge {bronze, silver, gold}

    export interface Game {
        name:string;
        rating: number;
        badge: Badge;
        attention: number;
        duration: string;
        recovery: number;
        impulsive?: number;
    }

    export enum Role {coach, student}

    export interface User {
        id? : number
        username : String
        role : Role
        passHash? : String
        passSalt? : String
        firstName : String
        lastName : String
        email : String
        phone : String
        dateJoined : Date
        coach? : User
        points? : number
        timeInPA? : number
        timeInGames? : number
        journalRequired? : boolean
        useBehaviorShaping? : boolean
        behaviorGoal? : any
        behavior1? : any
        behavior2? : any
        behavior3? : any
        behavior4? : any
        gamesPerSession? : number
    }


    export interface IDatabaseService {
        getUsers();
        getGames();
    }

    export class DatabaseService implements IDatabaseService{

        public static $inject = ['Loki'];

        private database : Loki;
        private users : LokiCollection<User>;
        private games : LokiCollection<Game>;

        getGames = () => this.games;
        getUsers = () => this.users;

        constructor(private loki) {
            this.database = new loki('play-attention.json', {autosave: true});
            this.users = this.database.addCollection<User>('users');
            this.games = this.database.addCollection<Game>('games');
        }
    }

}

