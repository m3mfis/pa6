var graphChart = [];

$(document).ready(function () {
    var graphsCount = $(".ct-chart").length;

    //init graphs on page load
    for (var i = 0; i < graphsCount; i++) {
        var data;
        var options;
        var type = $(".ct-chart").eq(i).data("type");
        $(".ct-chart").eq(i).addClass("chart" + i);


        if (type == "bar") {
            data = formDataBar(3);
            options = getOptionsBar();
        }
        else {
            data = formDataLine(3);
            options = getOptionsLine();
        }
        initGraph(type, data, options, ".chart" + i, i);
    }

    //change graph on dropdown selection
    $(".behavior-progress .cmn-dropdown .dropdown ul li").click(function () {

        var index = $(".behavior-progress").index($(this).closest(".behavior-progress"));

        graphChart[index].update.bind(graphChart[index]);
        var data;
        var options;
        var type = $(".ct-chart").eq(index).data("type");


        if (type == "bar") {
            data = formDataBar($(".behavior-progress .cmn-dropdown select").eq(index).val());
            options = getOptionsBar();
        }
        else {
            data = formDataLine($(".behavior-progress .cmn-dropdown select").eq(index).val());
            options = getOptionsLine();
        }
        initGraph(type, data, options, ".chart" + index, index);

    });
});


function initGraph(type, data, options, className, index) {
    if (type == "bar") {
        graphChart[index] = new Chartist.Bar(className, data, options);
    }
    else {
        graphChart[index] = new Chartist.Line(className, data, options);

    }
    onGraphDraw(graphChart[index]);
}

function onGraphDraw(chart) {


    chart.on('draw', function (data) {
        if (data.type === 'bar') {
            var color = ' ';
            if (data.seriesIndex == 0) {
                color = 'stroke: ' + chart.data.shadow[data.index];
            }
            else {
                color = 'stroke: ' + chart.data.color[data.index];
            }
            data.element.attr({
                style: ('stroke-width: 76px;' + color)
            });


            //add label into bar
            if (data.seriesIndex) {
                label = new Chartist.Svg("text");
                label.text(chart.data.series[data.seriesIndex][data.index]);
                label.attr({
                    x: data.x1,
                    y: data.y2 + 30,
                    "text-anchor": "middle",
                    "class": "custom-bar-label"
                });
                data.group.append(label);
            }



            data.element.animate({
                y2: {
                    dur: 1000,
                    from: data.y1,
                    to: data.y2,
                    easing: Chartist.Svg.Easing.easeOutQuint
                },
                opacity: {
                    dur: 1000,
                    from: 0,
                    to: 1,
                    easing: Chartist.Svg.Easing.easeOutQuint
                }
            });
        }
        else if (data.type === 'point') {

            if (data.index == 0) {
                circle = new Chartist.Svg('point', {
                    cx: [data.x], cy: [data.y], r: [1],
                }, 'ct-circle');
                data.element.replace(circle);
            }
            else {
                var line = new Chartist.Svg('line', {
                    x1: data.x, x2: data.x, y1: data.y, y2: data.y
                }, 'ct-point-border');
                data.element.replace(line);

                line = new Chartist.Svg('line', {
                    x1: data.x, x2: data.x, y1: data.y, y2: data.y
                }, 'ct-point');
                data.group.append(line);
            }
        }
        else if (data.type === 'grid') {
            if (data.index != 0) {
                var line;
                if (data.x1 === data.x2) {
                    line = new Chartist.Svg('line', {
                        x1: data.x1, x2: data.x2, y1: data.y2 - 12, y2: data.y2
                    }, 'ct-grid');
                }
                else {
                    line = new Chartist.Svg('line', {
                        x1: data.x1, x2: data.x1 + 15, y1: data.y1, y2: data.y2
                    }, 'ct-grid');
                }
                data.element.replace(line);
            }
        }
        else if (data.type === 'line') {
            data.element.animate({
                opacity: {
                    // The delay when we like to start the animation
                    begin: 0,
                    // Duration of the animation
                    dur: 1000,
                    // The value where the animation should start
                    from: 0,
                    // The value where it should end
                    to: 1
                }
            });
        }
    });
}



function formDataLine(ind) {
    var data;
    if (ind == 0) {
        data = {
            labels: ['30 June', '1 July', '2 July', '3 July', '4 July', ' '],
            series: [
                [15, 18, 29, 20, 16, 38]
            ]
        };
    }
    else if (ind == 1) {
        data = {
            labels: ['30 June', '1 July', '2 July', '3 July', '4 July', ' '],
            series: [
                [18, 29, 20, 16, 38, 15]
            ]
        };
    }
    else if (ind == 2) {
        data = {
            labels: ['30 June', '1 July', '2 July', '3 July', '4 July', ' '],
            series: [
                [29, 20, 16, 38, 15, 18]
            ]
        };
    }
    else {
        data = {
            labels: ['30 June', '1 July', '2 July', '3 July', '4 July', ' '],
            series: [
                [15, 25, 20, 28, 24, 35]
            ]
        };
    }
    return data;
}

function formDataBar(ind) {

    var data;
    if (ind == 0) {
        data = {
            labels: ['Attention Stamina', 'Visual Tracking', 'Academic Bridge', 'Time on Task'],
            series: [
                [4, 2, 5, 9],
                [4, 2, 5, 9]
            ],
            color: ["#7CC63C", "#708ABC", "#708ABC", "#708ABC"],
            shadow: ["#6FB236", "#6581AE", "#6581AE", "#6581AE"]
        };
    }
    else if (ind == 1) {
        data = {
            labels: ['Attention Stamina', 'Visual Tracking', 'Academic Bridge', 'Time on Task'],
            series: [
                [8, 6, 5, 2],
                [8, 6, 5, 2]
            ],
            color: ["#7CC63C", "#708ABC", "#708ABC", "#708ABC"],
            shadow: ["#6FB236", "#6581AE", "#6581AE", "#6581AE"]
        };
    }
    else if (ind == 2) {
        data = {
            labels: ['Attention Stamina', 'Visual Tracking', 'Academic Bridge', 'Time on Task'],
            series: [
                [4, 3, 7, 6],
                [4, 3, 7, 6]
            ],
            color: ["#7CC63C", "#708ABC", "#708ABC", "#708ABC"],
            shadow: ["#6FB236", "#6581AE", "#6581AE", "#6581AE"]
        };
    }
    else {
        data = {
            labels: ['Attention Stamina', 'Visual Tracking', 'Academic Bridge', 'Time on Task'],
            series: [
                [5, 6, 7, 4],
                [5, 6, 7, 4]
            ],
            color: ["#7CC63C", "#708ABC", "#708ABC", "#708ABC"],
            shadow: ["#6FB236", "#6581AE", "#6581AE", "#6581AE"]
        };
    }
    return data;
}

function getOptionsLine() {
    var options = {
        height: '92%',
        high: 50,
        low: 0,
        fullWidth: true,
        seriesBarDistance: -10,
        chartPadding: {
            right: 50
        },
        axisX: {
            showGrid: true
        },
        axisY: {
            showGrid: true,
            onlyInteger: true,
            labelInterpolationFnc: function (value, index) {
                return value % 10 === 0 || value == 0 ? value : null;
            }
        },
        lineSmooth: false
    };
    return options;
}

function getOptionsBar() {
    var options = {
        height: '95%',
        high: 10,
        low: 0,
        fullWidth: true,
        seriesBarDistance: -10,
        axisX: {
            showGrid: true
        },
        axisY: {
            showGrid: true,
            onlyInteger: true
        }
    };
    return options;
}