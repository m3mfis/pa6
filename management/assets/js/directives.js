angular.module('app.controllers')
    .directive('paChart', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                'pa6ChartType': '@chartType',
                'chartData': '=',
                'chartOptions': '=',
                'chartEvents': '='
            },
            controller: ['$scope', function link(scope) {

                scope.drawFn = function (data) {
                    if (data.type === 'bar') {
                        var color = ' ';
                        if (data.seriesIndex == 0) {
                            color = 'stroke: ' + scope.chartData.shadow[data.index];
                        }
                        else {
                            color = 'stroke: ' + scope.chartData.color[data.index];
                        }
                        data.element.attr({
                            style: ('stroke-width: 76px;' + color)
                        });


                        //add label into bar
                        if (data.seriesIndex) {
                            var label = new Chartist.Svg("text");
                            label.text(scope.chartData.series[data.seriesIndex][data.index]);
                            label.attr({
                                x: data.x1,
                                y: data.y2 + 30,
                                "text-anchor": "middle",
                                "class": "custom-bar-label"
                            });
                            data.group.append(label);
                        }


                        data.element.animate({
                            y2: {
                                dur: 1000,
                                from: data.y1,
                                to: data.y2,
                                easing: Chartist.Svg.Easing.easeOutQuint
                            },
                            opacity: {
                                dur: 1000,
                                from: 0,
                                to: 1,
                                easing: Chartist.Svg.Easing.easeOutQuint
                            }
                        });
                    }
                    else if (data.type === 'point') {

                        if (data.index == 0) {
                            var circle = new Chartist.Svg('point', {
                                cx: [data.x], cy: [data.y], r: [1]
                            }, 'ct-circle');
                            data.element.replace(circle);
                        }
                        else {
                            var line = new Chartist.Svg('line', {
                                x1: data.x, x2: data.x, y1: data.y, y2: data.y
                            }, 'ct-point-border');
                            data.element.replace(line);

                            line = new Chartist.Svg('line', {
                                x1: data.x, x2: data.x, y1: data.y, y2: data.y
                            }, 'ct-point');
                            data.group.append(line);
                        }
                    }
                    else if (data.type === 'grid') {
                        if (data.index != 0) {
                            var line;
                            if (data.x1 === data.x2) {
                                line = new Chartist.Svg('line', {
                                    x1: data.x1, x2: data.x2, y1: data.y2 - 12, y2: data.y2
                                }, 'ct-grid');
                            }
                            else {
                                line = new Chartist.Svg('line', {
                                    x1: data.x1, x2: data.x1 + 15, y1: data.y1, y2: data.y2
                                }, 'ct-grid');
                            }
                            data.element.replace(line);
                        }
                    }
                    else if (data.type === 'line') {
                        data.element.animate({
                            opacity: {
                                // The delay when we like to start the animation
                                begin: 0,
                                // Duration of the animation
                                dur: 1000,
                                // The value where the animation should start
                                from: 0,
                                // The value where it should end
                                to: 1
                            }
                        });
                    }
                }

            }],
            template:
            '<div class="graph" ng-class="{\'graph-bar\' : pa6ChartType == \'Bar\'}">' +
                '<chartist class="ct-chart ct-perfect-fourth chart0" ' +
                    'chartist-data="chartData" chartist-chart-type="{{pa6ChartType}}" ' +
                    'chartist-chart-options="chartOptions" chartist-events="{draw: drawFn }">' +
                '</chartist>' +
            '</div>'
        }
    });