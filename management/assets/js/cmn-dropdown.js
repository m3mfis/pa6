angular.element(document).ready(function () {

    var isShowing = false;
    var dropdownElement;
    $(document).on('click',".cmn-dropdown .dropdown .text",function () {
        dropdownElement = $(this).parent().parent();
        if (!isShowing) {
            $(dropdownElement).find(".dropdown ul").show();
        }
        else {
            $(dropdownElement).find(".dropdown ul").hide();
        }

        isShowing = !isShowing;
    });

    $(document).on('click',".cmn-dropdown .dropdown ul li",function () {
        $(dropdownElement).find(".dropdown ul").hide();
        isShowing = !isShowing;
    });

    $(document).click(function (e) {
        //hide dropdown list when clicked on document
        var container = $(".cmn-dropdown .dropdown");
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            $(".cmn-dropdown .dropdown ul").hide();
        }
    });
    //end - for fropdown
});
