var app;
(function (app) {
    app.translationsEN = {
        G_B_NERV_HAB: 'Engages in nervous habits <br> (e.g. twists-hair, bite nails, chew objects)',
        G_B_OFF_TASK: 'Off task (e.g. eyes moving off target)',
        G_B_CANNOT_ADJ: 'Cannot adjust behavior  <br> to expectations of situation',
        G_B_CALLS_OUT: 'Calls out',
        G_B_FIDGETS: 'Fidgets'
    };
})(app || (app = {}));
/// <reference path='_all.ts' />
var app;
(function (app) {
    'use strict';
    angular.module('app.controllers', ['lokijs',
        'ui.router',
        'ui.bootstrap',
        'rzModule',
        'ngSanitize',
        'angular-chartist',
        'pascalprecht.translate'])
        .service('userService', function () { return new app.UserService(); })
        .service('databaseService', ['Loki', function (loki) { return new app.DatabaseService(loki); }])
        .service('initDBService', ['databaseService', function (databaseService) { return new app.InitDBService(databaseService); }])
        .controller('ApplicationCtrl', app.ApplicationCtrl)
        .controller('ReviewCtrl', app.ReviewCtrl)
        .controller('GamesCtrl', app.GamesCtrl)
        .controller('RewardsCtrl', app.RewardsCtrl)
        .controller('ProfileCtrl', app.ProfileCtrl)
        .controller('GameResultsCtrl', app.GameResultsCtrl)
        .controller('BehaviorInputCtrl', app.BehaviorInputCtrl)
        .controller('CurrentResultCtrl', app.CurrentResultCtrl)
        .controller('LoginCtrl', app.LoginCtrl)
        .config(['$stateProvider', '$urlRouterProvider', '$translateProvider',
        function ($stateProvider, $urlRouterProvider, $translateProvider) {
            $translateProvider.translations('en', app.translationsEN);
            $translateProvider.preferredLanguage('en');
            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('login', {
                url: "/login",
                views: {
                    "app@": {
                        templateUrl: "assets/html/login/login.html",
                        controller: app.LoginCtrl
                    }
                },
                data: {
                    hideNav: true
                }
            })
                .state('app', {
                url: "/app",
                views: {
                    "app@": {
                        templateUrl: "assets/html/app.html",
                        controller: app.ApplicationCtrl
                    }
                }
            })
                .state('app.review', {
                url: "/review",
                views: {
                    "content@app": {
                        templateUrl: "assets/html/review/review.html",
                        controller: app.ReviewCtrl
                    },
                    "header@app": {
                        templateUrl: "assets/html/review/review-header.html"
                    }
                }
            })
                .state('app.games', {
                url: "/games",
                views: {
                    "content@app": {
                        templateUrl: "assets/html/games/games.html",
                        controller: app.GamesCtrl
                    },
                    "header@app": {
                        templateUrl: "assets/html/games/games-header.html"
                    }
                }
            })
                .state('app.games.game', {
                url: '/:game',
                views: {
                    "content@app": {
                        templateUrl: "assets/html/games/game-results.html",
                        controller: app.GameResultsCtrl
                    },
                    "header@app": {
                        templateUrl: "assets/html/games/game-results-header.html"
                    }
                }
            })
                .state('app.games.game.behavior', {
                url: '/behavior-input',
                views: {
                    "content@app": {
                        templateUrl: "assets/html/games/behavior-input.html",
                        controller: app.BehaviorInputCtrl
                    }
                },
                data: {
                    hideNav: true
                }
            })
                .state('app.games.game.currentResult', {
                url: '/current-result',
                views: {
                    "content@app": {
                        templateUrl: "assets/html/games/current-results.html",
                        controller: app.CurrentResultCtrl
                    },
                    "header@app": {
                        templateUrl: "assets/html/games/current-results-header.html"
                    }
                }
            })
                .state('app.rewards', {
                url: "/rewards",
                views: {
                    "content@app": {
                        templateUrl: "assets/html/rewards/rewards.html",
                        controller: app.RewardsCtrl
                    },
                    "header@app": {
                        templateUrl: "assets/html/rewards/rewards-header.html"
                    }
                }
            })
                .state('app.profile', {
                url: "/profile",
                views: {
                    "content@app": {
                        templateUrl: "assets/html/profile/profile.html",
                        controller: app.ProfileCtrl
                    },
                    "header@app": {
                        templateUrl: "assets/html/profile/profile-header.html"
                    }
                }
            });
        }])
        .run(['$rootScope', '$state', '$stateParams', 'initDBService',
        function ($rootScope, $state, $stateParams, initDB) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            initDB.init(app.Mode.test);
        }]);
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var LoginCtrl = (function () {
        function LoginCtrl($scope, $state, $stateParams, databaseService, userService) {
            var _this = this;
            this.$scope = $scope;
            this.$state = $state;
            this.$stateParams = $stateParams;
            this.databaseService = databaseService;
            this.userService = userService;
            this.usersCollection = databaseService.getUsers();
            $scope.signIn = function () {
                var user = _this.usersCollection.find({ username: $scope.login })[0];
                if (user) {
                    userService.setUser(user);
                    $state.go('app.review');
                }
            };
            $scope.assignCoach = function (coach) {
                $scope.coach = coach;
            };
            $scope.coaches =
                [{ name: 'Coach 1', id: 1 },
                    { name: 'Coach 2', id: 2 },
                    { name: 'Coach 3', id: 3 }];
        }
        LoginCtrl.$inject = ['$scope', '$state', '$stateParams', 'databaseService', 'userService'];
        return LoginCtrl;
    })();
    app.LoginCtrl = LoginCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var ApplicationCtrl = (function () {
        function ApplicationCtrl($scope, $state, $stateParams, $rootScope, userService, databaseService) {
            this.$scope = $scope;
            this.$state = $state;
            this.$stateParams = $stateParams;
            this.$rootScope = $rootScope;
            this.userService = userService;
            this.databaseService = databaseService;
            this.usersCollection = databaseService.getUsers();
            $scope.user = userService.getUser();
            if (!$scope.user) {
                $state.go('login');
            }
            else {
                $scope.coach = this.usersCollection.get($scope.user.coach);
            }
        }
        ApplicationCtrl.$inject = ['$scope', '$state', '$stateParams', '$rootScope', 'userService', 'databaseService'];
        return ApplicationCtrl;
    })();
    app.ApplicationCtrl = ApplicationCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var ReviewCtrl = (function () {
        function ReviewCtrl($scope) {
            this.$scope = $scope;
        }
        ReviewCtrl.$inject = ['$scope'];
        return ReviewCtrl;
    })();
    app.ReviewCtrl = ReviewCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var GamesCtrl = (function () {
        function GamesCtrl($scope, databaseService) {
            this.$scope = $scope;
            this.databaseService = databaseService;
            this.gamesCollection = databaseService.getGames();
            $scope.games = this.gamesCollection.data;
        }
        GamesCtrl.$inject = ['$scope', 'databaseService'];
        return GamesCtrl;
    })();
    app.GamesCtrl = GamesCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var GameResultsCtrl = (function () {
        function GameResultsCtrl($scope, $stateParams, databaseService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.databaseService = databaseService;
            this.options = {
                height: '92%',
                high: 50,
                low: 0,
                fullWidth: true,
                seriesBarDistance: -10,
                chartPadding: {
                    right: 50
                },
                axisX: {
                    showGrid: true
                },
                axisY: {
                    showGrid: true,
                    onlyInteger: true,
                    labelInterpolationFnc: function (value, index) {
                        return value % 10 === 0 || value == 0 ? value : null;
                    }
                },
                lineSmooth: false
            };
            this.gamesCollection = databaseService.getGames();
            $scope.game = this.gamesCollection.get($stateParams.game);
            $scope.lineData = {
                labels: ['30 June', '1 July', '2 July', '3 July', '4 July', ' '],
                series: [[15, 25, 20, 28, 24, 35]]
            };
            $scope.options = {
                height: '92%',
                high: 50,
                low: 0,
                fullWidth: true,
                seriesBarDistance: -10,
                chartPadding: {
                    right: 50
                },
                axisX: {
                    showGrid: true
                },
                axisY: {
                    showGrid: true,
                    onlyInteger: true,
                    labelInterpolationFnc: function (value, index) {
                        return value % 10 === 0 || value == 0 ? value : null;
                    }
                },
                lineSmooth: false
            };
        }
        GameResultsCtrl.$inject = ['$scope', '$stateParams', 'databaseService'];
        return GameResultsCtrl;
    })();
    app.GameResultsCtrl = GameResultsCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var BehaviorInputCtrl = (function () {
        function BehaviorInputCtrl($scope, $stateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.startGame = function () {
                console.log("start game");
                var process = ProcessManager.startProcess();
                setInterval(function () { return process.sendMessage({ state: 'STOP' }); }, 1000);
                process.setOnDataCallback(function (data) { return console.log(data); });
            };
            $scope.game = $stateParams.game;
            $scope.behaviors = [
                {
                    text: 'G_B_NERV_HAB',
                    value: 0,
                    min: 0,
                    max: 40
                }, {
                    text: 'G_B_OFF_TASK',
                    value: 700,
                    min: 0,
                    max: 900
                }, {
                    text: 'G_B_CANNOT_ADJ',
                    value: 50,
                    min: 0,
                    max: 100
                }, {
                    text: 'G_B_CALLS_OUT',
                    value: 0,
                    min: 0,
                    max: 120
                }, {
                    text: 'G_B_FIDGETS',
                    value: 0,
                    min: 0,
                    max: 100
                }
            ];
            $scope.startGame = this.startGame;
            console.log($stateParams, 'behavior input');
        }
        BehaviorInputCtrl.$inject = ['$scope', '$stateParams'];
        return BehaviorInputCtrl;
    })();
    app.BehaviorInputCtrl = BehaviorInputCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var CurrentResultCtrl = (function () {
        function CurrentResultCtrl($scope, $stateParams, databaseService) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
            this.databaseService = databaseService;
            this.options = {
                height: '95%',
                high: 10,
                low: 0,
                fullWidth: true,
                seriesBarDistance: -10,
                axisX: {
                    showGrid: true
                },
                axisY: {
                    showGrid: true,
                    onlyInteger: true
                }
            };
            this.gamesCollection = databaseService.getGames();
            $scope.game = this.gamesCollection.get($stateParams.game);
            $scope.options = this.options;
            $scope.barData = {
                labels: ['Attention Stamina', 'Visual Tracking', 'Academic Bridge', 'Time on Task'],
                series: [[5, 6, 7, 4], [5, 6, 7, 4]],
                color: ["#7CC63C", "#708ABC", "#708ABC", "#708ABC"],
                shadow: ["#6FB236", "#6581AE", "#6581AE", "#6581AE"]
            };
        }
        CurrentResultCtrl.$inject = ['$scope', '$stateParams', 'databaseService'];
        return CurrentResultCtrl;
    })();
    app.CurrentResultCtrl = CurrentResultCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var RewardsCtrl = (function () {
        function RewardsCtrl($scope, $stateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
        }
        RewardsCtrl.$inject = ['$scope', '$stateParams'];
        return RewardsCtrl;
    })();
    app.RewardsCtrl = RewardsCtrl;
})(app || (app = {}));
/// <reference path='../_all.ts' />
var app;
(function (app) {
    'use strict';
    var ProfileCtrl = (function () {
        function ProfileCtrl($scope, $stateParams) {
            this.$scope = $scope;
            this.$stateParams = $stateParams;
        }
        ProfileCtrl.$inject = ['$scope', '$stateParams'];
        return ProfileCtrl;
    })();
    app.ProfileCtrl = ProfileCtrl;
})(app || (app = {}));
var app;
(function (app) {
    var UserService = (function () {
        function UserService() {
        }
        UserService.prototype.setUser = function (user) {
            this.user = user;
            localStorage.setItem('user', JSON.stringify(user));
        };
        UserService.prototype.getUser = function () {
            this.user = JSON.parse(localStorage.getItem('user'));
            return this.user;
        };
        return UserService;
    })();
    app.UserService = UserService;
})(app || (app = {}));
var app;
(function (app) {
    (function (Badge) {
        Badge[Badge["bronze"] = 0] = "bronze";
        Badge[Badge["silver"] = 1] = "silver";
        Badge[Badge["gold"] = 2] = "gold";
    })(app.Badge || (app.Badge = {}));
    var Badge = app.Badge;
    (function (Role) {
        Role[Role["coach"] = 0] = "coach";
        Role[Role["student"] = 1] = "student";
    })(app.Role || (app.Role = {}));
    var Role = app.Role;
    var DatabaseService = (function () {
        function DatabaseService(loki) {
            var _this = this;
            this.loki = loki;
            this.getGames = function () { return _this.games; };
            this.getUsers = function () { return _this.users; };
            this.database = new loki('play-attention.json', { autosave: true });
            this.users = this.database.addCollection('users');
            this.games = this.database.addCollection('games');
        }
        DatabaseService.$inject = ['Loki'];
        return DatabaseService;
    })();
    app.DatabaseService = DatabaseService;
})(app || (app = {}));
var app;
(function (app) {
    (function (Mode) {
        Mode[Mode["test"] = 0] = "test";
    })(app.Mode || (app.Mode = {}));
    var Mode = app.Mode;
    var InitDBService = (function () {
        function InitDBService(databaseService) {
            var _this = this;
            this.databaseService = databaseService;
            this.init = function (mode) {
                switch (mode) {
                    case Mode.test:
                        _this.users.removeDataOnly();
                        _this.games.removeDataOnly();
                        _this.users.insert([
                            {
                                username: "coach",
                                role: app.Role.coach,
                                password: '123',
                                firstName: 'Fernando',
                                lastName: "Lewis",
                                email: "some@mail",
                                phone: '777-77-77',
                                dateJoined: new Date()
                            }]);
                        _this.users.insert([
                            {
                                username: "stud",
                                role: app.Role.student,
                                password: '123',
                                firstName: 'Julian',
                                lastName: "Davis",
                                email: "some1@mail",
                                phone: '777-77-77',
                                dateJoined: new Date(),
                                coach: _this.users.data[0]['$loki'],
                                points: 27,
                                timeInPA: 123123,
                                timeInGames: 32133,
                                journalRequierd: true,
                                gamesPerSession: 2
                            }
                        ]);
                        _this.games.insert([
                            {
                                name: 'Ocean Quest',
                                rating: 3,
                                badge: app.Badge.bronze,
                                attention: 75,
                                duration: "3:42",
                                recovery: 4,
                                impulsive: 2
                            },
                            {
                                name: 'Memory Lane',
                                rating: 4,
                                badge: app.Badge.silver,
                                attention: 50,
                                duration: "3:42",
                                recovery: 3
                            },
                            {
                                name: 'One more',
                                rating: 5,
                                badge: app.Badge.gold,
                                attention: 99,
                                duration: "3:42",
                                recovery: 6,
                                impulsive: 3
                            }]);
                }
            };
            this.users = databaseService.getUsers();
            this.games = databaseService.getGames();
        }
        InitDBService.$inject = ['databaseService'];
        return InitDBService;
    })();
    app.InitDBService = InitDBService;
})(app || (app = {}));
//#### Include type definitions ####
/// <reference path='typings/tsd.d.ts' />
//### Protobuf d.ts ###
/// <reference path='api/GameApi.d.ts' />
//### Translation tables ###
/// <reference path='Translate.ts' />
//#### Application ###
/// <reference path='Application.ts'/>
//#### External modules(node-specific) ###
/// <reference path='external/Process.ts'/>
/// <reference path='external/nodejs/ProcessManager.ts'/>
//#### Controllers ####
/// <reference path='controllers/LoginCtrl.ts' />
/// <reference path='controllers/ApplicationCtrl.ts' />
/// <reference path='controllers/ReviewCtrl.ts' />
/// <reference path='controllers/GamesCtrl.ts' />
/// <reference path='controllers/GameResultsCtrl.ts' />
/// <reference path='controllers/BehaviorInputCtrl.ts' />
/// <reference path='controllers/CurrentResultCtrl.ts' />
/// <reference path='controllers/RewardsCtrl.ts' />
/// <reference path='controllers/ProfileCtrl.ts' />
//#### Services ####
/// <reference path='services/UserService.ts' />
/// <reference path='services/DatabaseService.ts' />
/// <reference path='services/InitDBService.ts' />
